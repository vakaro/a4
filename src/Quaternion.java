import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Quaternions. Basic operations.
 */
public class Quaternion {

    private final double real;
    private final double i;
    private final double j;
    private final double k;
    private final double maxPositive = 0.0000000000001;

    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        // https://en.wikipedia.org/wiki/Quaternion
        // https://gist.github.com/mxrguspxrt/4044808
        this.real = a;
        this.i = b;
        this.j = c;
        this.k = d;
    }

    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    public double getRpart() {
        return real;
    }

    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    public double getIpart() {
        return i;
    }

    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    public double getJpart() {
        return j;
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    public double getKpart() {
        return k;
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        String s = "+" + real + "+" + i + "i" + "+" + j + "j" + "+" + k + "k";
        s = s.replaceAll("\\+-", "-");
        return s;
    }

    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */
    public static Quaternion valueOf(String s) {

        if (s == null) {
            throw new NullPointerException("valueOf method input must be string");
        }

        s = s.trim();
        if (s.equals("")) {
            throw new IllegalArgumentException("Invalid : input is empty string");
        }

        String originalString = s;

        s = s.replaceAll(" ", "");
        s = s.replaceAll("\\+-", "-");
        s = s.replaceAll("-+ ", "+");

//        String or = "|";
//        String digit = "\\d+";
//        String dot = "\\.";
//        String oper = "[+-]";
//        String operQuestion = "[+-]?";
//
//        String digitPart = "(" + digit + or + digit + dot + or + dot + digit + or + digit + dot + digit + ")";
//        String floatPart = "(" + digitPart + or + digitPart + "[eE]" + "[+-]?" + digit + ")";
//        String doublePart = "(" + floatPart + or + floatPart + "[dD]" + ")";
//        String real = "([+-]?" + doublePart + ")";
//        String i = "(" + oper + doublePart + "[iI]" + or + operQuestion + "[iI]" + ")";
//        String j = "(" + oper + doublePart + "[jJ]" + or + operQuestion + "[jJ]" + ")";
//        String k = "(" + oper + doublePart + "[kK]" + or + operQuestion + "[kK]" + ")";
//
//        String expr2 = "(" + real +
//                or + real + i +
//                or + real + i + j +
//                or + real + i + j + k +
//                or + real + i + k +
//                or + real + j +
//                or + real + j + k +
//                or + real + k +
//                or + i +
//                or + i + j +
//                or + i + j + k +
//                or + i + k +
//                or + j +
//                or + j + k +
//                or + k + ")";

        // https://regex101.com/
        // for testing
        //  -2.0e-3-4.0e-1i-5.0e-2j-6.0e3k
        //  2.2+5.0+7.0+9.2
        //  2.2+5.0i+7.0j+9.2k
        //  -1-2i-3j-4k

        String expr = "([+-]?\\d*\\.?\\d+?[eE]?[-+]?\\d*)([+-]\\d*\\.?\\d+?[eE]?[-+]?\\d*[iI]?)([+-]\\d*\\.?\\d+?[eE]?[-+]?\\d*[jJ]?)([+-]\\d*\\.?\\d+?[eE]?[-+]?\\d*[kK]?)";
        Pattern p = Pattern.compile("^" + expr + "$");
        Matcher m = p.matcher(s);

        if (m.find()) {

            if (m.groupCount() != 4) {
                throw new IllegalArgumentException("Not enough matching numbers in string" + "\n" + originalString);
            }

            try {
                String iTrimmed = m.group(2).replaceFirst("i", "");
                String jTrimmed = m.group(3).replaceFirst("j", "");
                String kTrimmed = m.group(4).replaceFirst("k", "");
                double realV = Double.parseDouble(m.group(1));
                double iV = Double.parseDouble(iTrimmed);
                double jV = Double.parseDouble(jTrimmed);
                double kV = Double.parseDouble(kTrimmed);
                return new Quaternion(realV, iV, jV, kV);
            } catch (RuntimeException err) {
                throw new IllegalArgumentException("Invalid character in string " + originalString);
            }
        } else {
            throw new RuntimeException("String does not match Quaternion rules! Input: " +  originalString);
        }
    }

    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Quaternion(real, i, j, k);
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {

        return Math.abs(real) < maxPositive && Math.abs(i) < maxPositive && Math.abs(j) < maxPositive && Math.abs(k) < maxPositive;

    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(a+bi+cj+dk) = a-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        return new Quaternion(real, -i, -j, -k);
    }

    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(a+bi+cj+dk) = -a-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        return new Quaternion(-real, -i, -j, -k);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(q.real + real, i + q.i, j + q.j, k + q.k);
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {
        double tempReal = real * q.real - i * q.i - j * q.j - k * q.k;
        double tempI = real * q.i + q.real * i + j * q.k - k * q.j;
        double tempJ = real * q.j - i * q.k + j * q.real + k * q.i;
        double tempK = real * q.k + i * q.j - j * q.i + k * q.real;
        return new Quaternion(tempReal, tempI, tempJ, tempK);
    }

    /**
     * Multiplication by a coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        return new Quaternion(real * r, i * r, j * r, k * r);
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     * ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     *
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        if (this.isZero()) {
            throw new RuntimeException("Quaternion equals to zero and so can't be inversed!");
        }
        double div = real * real + i * i + j * j + k * k;
        return new Quaternion(real / div, (-i) / div, (-j) / div, (-k) / div);
    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        return plus(q.opposite());
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        if (q == null || q.isZero()) {
            throw new IllegalArgumentException("Can't divide by null or zero!");
        }
        return times(q.inverse());
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        if (q == null || q.isZero()) {
            throw new IllegalArgumentException("Can't divide by null or zero!");
        }
        return q.inverse().times(this);
    }

    /**
     * Equality test of quaternions. Difference of equal numbers
     * is (close to) zero.
     *
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @Override
    public boolean equals(Object qo) {
        if (qo == null) {
            throw new NullPointerException("Comparable is null");
        }
        if (!(qo instanceof Quaternion)) {
            throw new ClassCastException("Comparable is not of type Quaternion");
        }

        Quaternion quart = (Quaternion) qo;

        if (Math.abs(quart.real - real) > maxPositive) {
            return false;
        }
        if (Math.abs(quart.i - i) > maxPositive) {
            return false;
        }
        if (Math.abs(quart.j - j) > maxPositive) {
            return false;
        }
        return Math.abs(quart.k - k) < maxPositive;
    }

    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult(Quaternion q) {

        return times(q.conjugate()).plus(q.times(conjugate())).times(0.5);

    }

    /**
     * Integer hashCode has to be the same for equal objects.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        // https://stackoverflow.com/questions/11597386/objects-hash-vs-objects-hashcode-clarification-needed
        return Objects.hash(real, i, j, k);
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     *
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(real * real + i * i + j * j + k * k);
    }

    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */
    public static void main(String[] arg) {
//        Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
//        if (arg.length > 0)
//            arv1 = valueOf(arg[0]);
//        System.out.println("first: " + arv1.toString());
//        System.out.println("real: " + arv1.getRpart());
//        System.out.println("imagi: " + arv1.getIpart());
//        System.out.println("imagj: " + arv1.getJpart());
//        System.out.println("imagk: " + arv1.getKpart());
//        System.out.println("isZero: " + arv1.isZero());
//        System.out.println("conjugate: " + arv1.conjugate());
//        System.out.println("opposite: " + arv1.opposite());
//        System.out.println("hashCode: " + arv1.hashCode());
//        Quaternion res = null;
//        try {
//            res = (Quaternion) arv1.clone();
//        } catch (CloneNotSupportedException e) {
//        }
//        ;
//        System.out.println("clone equals to original: " + res.equals(arv1));
//        System.out.println("clone is not the same object: " + (res != arv1));
//        System.out.println("hashCode: " + res.hashCode());
//        res = valueOf(arv1.toString());
//        System.out.println("string conversion equals to original: "
//                + res.equals(arv1));
//        Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
//        if (arg.length > 1)
//            arv2 = valueOf(arg[1]);
//        System.out.println("second: " + arv2.toString());
//        System.out.println("hashCode: " + arv2.hashCode());
//        System.out.println("equals: " + arv1.equals(arv2));
//        res = arv1.plus(arv2);
//        System.out.println("plus: " + res);
//        System.out.println("times: " + arv1.times(arv2));
//        System.out.println("minus: " + arv1.minus(arv2));
//        double mm = arv1.norm();
//        System.out.println("norm: " + mm);
//        System.out.println("inverse: " + arv1.inverse());
//        System.out.println("divideByRight: " + arv1.divideByRight(arv2));
//        System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
//        System.out.println("dotMult: " + arv1.dotMult(arv2));
//
//        // System.out.println(valueOf("-2.1e-2-3.2e-1-4.3e+2-5.4e2"));
//        // System.out.println(valueOf("-2.1e-2-3.2e-1i-4.3e+2j-5.4e2kk"));
//        // System.out.println(valueOf("-2.1e-2-3.2e-1i-4.3e+2j-5.4e2k+"));
//
//        System.out.println("====================");

//        System.out.println (valueOf("2+3i+4j+5k"));
//        System.out.println (valueOf("-2-3i-4j-5k"));
//        System.out.println (valueOf("-2.0e-3-4.0e-1i-5.0e-2j-6.0e-3k"));
//        System.out.println(valueOf("     "));
//        System.out.println (valueOf("2+3i+4j+5kk"));
//        System.out.println (valueOf("2+3i+4j+5k+"));
        System.out.println (valueOf("2+3j+4i+5k"));
    }
}